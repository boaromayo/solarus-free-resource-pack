--[[
	This unit test for the multi_events script tests various combinations of meta and
	non-meta events (part 1).
]]

local map = ...
local game = map:get_game()
local hero = game:get_hero()

require"scripts/multi_events"
local events_proto = require"tests/events_prototype"

function map:on_opening_transition_finished()
	--Test #1: define meta event after registering event
	local sprite1 = sol.sprite.create"hero/tunic1"
	local function sprite1_trigger()
		sprite1:set_direction(1 - sprite1:get_direction()) --toggle between direction 1 & 0
	end
	events_proto:set_trigger(sprite1_trigger)
	sprite1:register_event("on_direction_changed", function()
		events_proto:log"sprite1a"
	end)
	sprite1:register_event("on_direction_changed", function()
		events_proto:log"sprite1b"
	end)
	
	local sprite_meta = sol.main.get_metatable"sprite"
	function sprite_meta:on_direction_changed() events_proto:log"unregistered_meta" end
	events_proto:trigger"unregistered_meta;sprite1a;sprite1b"
	
	
	--Test #2: register an additional events after defining meta event
	sprite1:register_event("on_direction_changed", function()
		events_proto:log"sprite1c"
	end, true)
	events_proto:trigger"sprite1c;unregistered_meta;sprite1a;sprite1b"
	
	sprite1:register_event("on_direction_changed", function()
		events_proto:log"sprite1d"
	end)
	events_proto:trigger"sprite1c;unregistered_meta;sprite1a;sprite1b;sprite1d"
	
	
	--Test #3: unregistered meta event called when no event defined
	local sprite2 = sol.sprite.create"hero/tunic1"
	local function sprite2_trigger()
		sprite2:set_direction(1 - sprite2:get_direction()) --toggle between direction 1 & 0
	end
	events_proto:set_trigger(sprite2_trigger)
	events_proto:trigger"unregistered_meta"
	
	
	--Test #4: register events after defining meta event
	sprite2:register_event("on_direction_changed", function()
		events_proto:log"sprite2a"
	end)
	events_proto:trigger"unregistered_meta;sprite2a"
	
	sprite2:register_event("on_direction_changed", function()
		events_proto:log"sprite2b"
	end, true)
	events_proto:trigger"sprite2b;unregistered_meta;sprite2a"
	
	sprite2:register_event("on_direction_changed", function()
		events_proto:log"sprite2c"
	end)
	events_proto:trigger"sprite2b;unregistered_meta;sprite2a;sprite2c"
	
	
	--Test #5: register events in-front when meta event already defined
	local sprite3 = sol.sprite.create"hero/tunic1"
	local function sprite3_trigger()
		sprite3:set_direction(1 - sprite3:get_direction()) --toggle between direction 1 & 0
	end
	events_proto:set_trigger(sprite3_trigger)
	sprite3:register_event("on_direction_changed", function()
		events_proto:log"sprite3a"
	end, true)
	events_proto:trigger"sprite3a;unregistered_meta"
	
	sprite3:register_event("on_direction_changed", function()
		events_proto:log"sprite3b"
	end)
	events_proto:trigger"sprite3a;unregistered_meta;sprite3b"
	
	
	--Test #6: change meta event
	function sprite_meta:on_direction_changed() events_proto:log"unregistered_meta2" end
	events_proto:trigger"sprite3a;unregistered_meta2;sprite3b"
	
	events_proto:set_trigger(sprite2_trigger)
	events_proto:trigger"sprite2b;unregistered_meta2;sprite2a;sprite2c"
	
	events_proto:set_trigger(sprite1_trigger)
	events_proto:trigger"sprite1c;unregistered_meta2;sprite1a;sprite1b;sprite1d"
	
	
	--Test #7 register another meta event
	sprite_meta:register_event("on_direction_changed", function()
		events_proto:log"meta3"
	end)
	events_proto:trigger"sprite1c;unregistered_meta2;meta3;sprite1a;sprite1b;sprite1d"
	
	events_proto:set_trigger(sprite2_trigger)
	events_proto:trigger"sprite2b;unregistered_meta2;meta3;sprite2a;sprite2c"
	
	events_proto:set_trigger(sprite3_trigger)
	events_proto:trigger"sprite3a;unregistered_meta2;meta3;sprite3b"
	
	
	--Test #8 register another event after registered meta event
	sprite3:register_event("on_direction_changed", function()
		events_proto:log"sprite3c"
	end, true)
	events_proto:trigger"sprite3c;sprite3a;unregistered_meta2;meta3;sprite3b"
	
	sprite2:register_event("on_direction_changed", function()
		events_proto:log"sprite2d"
	end)
	events_proto:set_trigger(sprite2_trigger)
	events_proto:trigger"sprite2b;unregistered_meta2;meta3;sprite2a;sprite2c;sprite2d"
	
	
	--Test #9 registered meta event called when no event defined
	local sprite4 = sol.sprite.create"hero/tunic1"
	local function sprite4_trigger()
		sprite4:set_direction(1 - sprite4:get_direction()) --toggle between direction 1 & 0
	end
	events_proto:set_trigger(sprite4_trigger)
	events_proto:trigger"unregistered_meta2;meta3"
	
	
	events_proto:exit()
end
