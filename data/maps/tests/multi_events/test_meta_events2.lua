--[[
	This unit test for the multi_events script tests various combinations of meta and
	non-meta events (part 2).
]]

local map = ...
local game = map:get_game()
local hero = game:get_hero()

require"scripts/multi_events"
local events_proto = require"tests/events_prototype"

function map:on_opening_transition_finished()
	--Test #1: register meta event after registering event (both in-front and in-back)
	local sprite1 = sol.sprite.create"hero/tunic1"
	sprite1:register_event("on_direction_changed", function()
		events_proto:log"sprite1a"
	end)
	local function sprite1_trigger(callback)
		sprite1:set_direction(1 - sprite1:get_direction()) --toggle between direction 1 & 0
	end
	events_proto:set_trigger(sprite1_trigger)
	events_proto:trigger"sprite1a"
	
	local sprite2 = sol.sprite.create"hero/tunic1"
	sprite2:register_event("on_direction_changed", function()
		events_proto:log"sprite2a"
	end, true)
	local function sprite2_trigger()
		sprite2:set_direction(1 - sprite2:get_direction()) --toggle between direction 1 & 0
	end
	events_proto:set_trigger(sprite2_trigger)
	events_proto:trigger"sprite2a"
	
	local sprite_meta = sol.main.get_metatable"sprite"
	sprite_meta:register_event("on_direction_changed", function()
		events_proto:log"sprite_meta1"
	end)
	events_proto:trigger"sprite2a;sprite_meta1"
	
	events_proto:set_trigger(sprite1_trigger)
	events_proto:trigger"sprite_meta1;sprite1a"
	
	
	--Test #2: register additional events following #1
	sprite1:register_event("on_direction_changed", function() events_proto:log"sprite1b" end, true)
	events_proto:trigger"sprite1b;sprite_meta1;sprite1a"
	
	sprite1:register_event("on_direction_changed", function() events_proto:log"sprite1c" end)
	events_proto:trigger"sprite1b;sprite_meta1;sprite1a;sprite1c"
	
	events_proto:set_trigger(sprite2_trigger)
	sprite2:register_event("on_direction_changed", function() events_proto:log"sprite2b" end)
	events_proto:trigger"sprite2a;sprite_meta1;sprite2b"
	
	sprite2:register_event("on_direction_changed", function() events_proto:log"sprite2c" end, true)
	events_proto:trigger"sprite2c;sprite2a;sprite_meta1;sprite2b"
	
	
	--Test #3: already registered meta event (both in-front and in-back)
	local sprite3 = sol.sprite.create"hero/tunic1"
	local function sprite3_trigger()
		sprite3:set_direction(1 - sprite3:get_direction()) --toggle between direction 1 & 0
	end
	events_proto:set_trigger(sprite3_trigger)
	sprite3:register_event("on_direction_changed", function() events_proto:log"sprite3a" end)
	events_proto:trigger"sprite_meta1;sprite3a"
	
	sprite3:register_event("on_direction_changed", function() events_proto:log"sprite3b" end, true)
	events_proto:trigger"sprite3b;sprite_meta1;sprite3a"
	
	local sprite4 = sol.sprite.create"hero/tunic1"
	local function sprite4_trigger()
		sprite4:set_direction(1 - sprite4:get_direction()) --toggle between direction 1 & 0
	end
	events_proto:set_trigger(sprite4_trigger)
	sprite4:register_event("on_direction_changed", function()
		events_proto:log"sprite4a"
	end, true)
	events_proto:trigger"sprite4a;sprite_meta1"
	
	sprite4:register_event("on_direction_changed", function()
		events_proto:log"sprite4b"
	end)
	events_proto:trigger"sprite4a;sprite_meta1;sprite4b"
	
	
	--Test #4: register additional meta event
	sprite_meta:register_event("on_direction_changed", function()
		events_proto:log"sprite_meta2"
	end, true)
	events_proto:trigger"sprite4a;sprite_meta2;sprite_meta1;sprite4b"
	
	events_proto:set_trigger(sprite3_trigger)
	events_proto:trigger"sprite3b;sprite_meta2;sprite_meta1;sprite3a"
	
	events_proto:set_trigger(sprite2_trigger)
	events_proto:trigger"sprite2c;sprite2a;sprite_meta2;sprite_meta1;sprite2b"
	
	events_proto:set_trigger(sprite1_trigger)
	events_proto:trigger"sprite1b;sprite_meta2;sprite_meta1;sprite1a;sprite1c"
	
	
	events_proto:exit()
end
