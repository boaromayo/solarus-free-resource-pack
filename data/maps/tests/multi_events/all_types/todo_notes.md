These objects still need unit tests created for use with the multi_events script.

1. enemy
- TODO
1. movement
- ??? How does one create a generic movement object?
1. straight_movement
- TODO
1. target_movement
- TODO
1. random_movement
- TODO
1. path_movement
- TODO
1. random_path_movement
- TODO
1. path_finding_movement
- TODO
1. circle_movement
- TODO
1. jump_movement
- TODO
1. pixel_movement
- TODO
1. surface
- no native events
1. text_surface
- no native events
1. timer
- no native events
1. dynamic_tile
- TODO no native events, use inherited one
1. pickable
- TODO no native events, use inherited one
1. block
- TODO no native events, use inherited one
1. jumper
- TODO no native events, use inherited one
1. wall
- TODO no native events, use inherited one
1. crystal
- TODO no native events, use inherited one
1. crystal_block
- TODO no native events, use inherited one
1. stream
- TODO no native events, use inherited one
1. stairs
- TODO no native events, use inherited one
1. bomb
- TODO no native events, use inherited one
1. explosion
- TODO no native events, use inherited one
1. fire
- TODO no native events, use inherited one
1. arrow
- TODO no native events, use inherited one
1. hookshot
- TODO no native events, use inherited one
1. boomerang
- TODO no native events, use inherited one
1. shop_treasure
- TODO this one is hard... could use inherited event
