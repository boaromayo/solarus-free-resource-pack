local map = ...
local game = map:get_game()

require("scripts/coroutine_helper")

function map:on_started()
  game:set_max_life(12)
  game:set_life(game:get_max_life())
  game:set_ability("sword", 1)
end

function map:on_opening_transition_finished()

  map:start_coroutine(function()
    -- TODO
    -- check that they create eggs
    -- check they shoot fireballs
    wait(10000)
    sol.main.exit()
  end)
end
